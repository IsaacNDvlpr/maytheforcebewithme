﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobileCodeChallenge.Models;
using MobileCodeChallenge.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileCodeChallenge.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StarshipView : ContentPage
    {
        public StarshipView(Starship selectedStarship)
        {
            InitializeComponent();
            BindingContext = new StarshipViewModel(selectedStarship);
            Title = selectedStarship.Name;
        }

        protected async override void OnAppearing()
        {
            //Do not load data while initializing, instead load when page appears
            var grabStarshipVMBindingContext = BindingContext as StarshipViewModel;
            await grabStarshipVMBindingContext.LoadStarshipImage();
            grabStarshipVMBindingContext.ActivityIndicatorRun = false;
        }
    }
}
