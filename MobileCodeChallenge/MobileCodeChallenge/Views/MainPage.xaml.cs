﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobileCodeChallenge.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileCodeChallenge
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(Navigation);
        }

        protected async override void OnAppearing()
        {
            //Do not load data while initializing, instead load when page appears
            var grabMainPageVMBindingContext = BindingContext as MainPageViewModel;
            await grabMainPageVMBindingContext.LoadStarshipsAsync();
            grabMainPageVMBindingContext.ActivityIndicatorRun = false;
        }

    }
}
