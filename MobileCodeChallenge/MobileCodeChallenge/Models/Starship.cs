﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileCodeChallenge.Models
{
    public class Starship
    {
        public string Name { get; set; }
        public string Model { get; set; }
        public string StarshipClass { get; set; }
        public string Manufacturer { get; set; }
        public string CostInCredits { get; set; }
        public string Length { get; set; } //meters
        public string Crew { get; set; }
        public string Passengers { get; set; }
        public string MaxAtmospheringSpeed { get; set; }
        public string HyperdriveRating { get; set; }
        public string MGLT { get; set; } //megalights per hour
        public string CargoCapacity { get; set; } //kilograms
        public string Consumables { get; set; }
    }
}
