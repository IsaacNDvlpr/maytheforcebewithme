﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MobileCodeChallenge.Models;
using Newtonsoft.Json;

namespace MobileCodeChallenge.Services
{
    public class StarWarsApiService
    {
        HttpClient _client;

        public StarWarsApiService()
        {
            _client = new HttpClient();
        }

        public async Task<List<Starship>> GetAllStarships()
        {
            var baseUri = "https://swapi.co/api/starships/";
            var uri = new Uri(baseUri);

            var response = await _client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsStringAsync();
                var jSonParse = JsonConvert.DeserializeObject<StarshipJsonDeserialize>(result);

                if (jSonParse != null)
                {
                    //Only returns 10 results per page, iterate through all pages
                    double howManyPages = (double)jSonParse.count / jSonParse.results.Count;
                    howManyPages = Math.Ceiling(howManyPages);
                    List<Starship> listStarships = new List<Starship>();
                    for (int i = 1; i <= howManyPages; i++)
                    {
                        var baseUriTwo = "https://swapi.co/api/starships/?page=";
                        var uriTwo = new Uri(baseUriTwo + i);
                        var responseTwo = await _client.GetAsync(uriTwo);
                        if (responseTwo.IsSuccessStatusCode)
                        {
                            var resultTwo = await responseTwo.Content.ReadAsStringAsync();
                            var jSonParseTwo = JsonConvert.DeserializeObject<StarshipJsonDeserialize>(resultTwo);
                            if (jSonParseTwo != null)
                            {
                                foreach (var item in jSonParseTwo.results)
                                {
                                    var addStarship = new Starship
                                    {
                                        Name = item.name,
                                        Model = item.model,
                                        StarshipClass = item.starship_class,
                                        Manufacturer = item.manufacturer,
                                        CostInCredits = item.cost_in_credits,
                                        Length = item.length,
                                        Crew = item.crew,
                                        Passengers = item.passengers,
                                        MaxAtmospheringSpeed = item.max_atmosphering_speed,
                                        HyperdriveRating = item.hyperdrive_rating,
                                        MGLT = item.MGLT,
                                        CargoCapacity = item.cargo_capacity,
                                        Consumables = item.consumables
                                    };
                                    listStarships.Add(addStarship);
                                }
                            }
                        }
                    }
                    return listStarships;
                }
                else
                {
                    Console.WriteLine("There are no starships available in database");
                    return null;
                }
            }
            else
            {
                Console.WriteLine("Unable to pull data from SWAPI: " + response.ReasonPhrase);
                return null;
            }
        }
    }
}
