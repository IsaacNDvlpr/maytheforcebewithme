﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using MobileCodeChallenge.Models;
using MobileCodeChallenge.Services;
using MobileCodeChallenge.Views;
using Xamarin.Forms;

namespace MobileCodeChallenge.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        private INavigation _navigation;

        private bool activityIndicatorRun;
        public bool ActivityIndicatorRun
        {
            get
            {
                return activityIndicatorRun;
            }
            set
            {
                if (activityIndicatorRun != value)
                {
                    activityIndicatorRun = value;
                    OnPropertyChanged("ActivityIndicatorRun");
                }
            }
        }

        private ObservableCollection<Starship> starshipOc;
        public ObservableCollection<Starship> StarshipOc
        {
            set
            {
                if (starshipOc != value)
                {
                    starshipOc = value;
                    OnPropertyChanged("StarshipOc");
                }
            }
            get
            {
                return starshipOc;
            }
        }
        private Starship listViewItemSelectedBinding;
        public Starship ListViewItemSelectedBinding
        {
            set
            {
                if (listViewItemSelectedBinding != value)
                {
                    listViewItemSelectedBinding = value;
                    OnPropertyChanged("ListViewItemSelectedBinding");
                    HandleSelectedItem();
                }
            }
            get
            {
                return listViewItemSelectedBinding;
            }
        }
        public MainPageViewModel(INavigation navigation)
        {
            _navigation = navigation;
            ActivityIndicatorRun = true;
        }

        public async Task LoadStarshipsAsync()
        {
            var grabStarships = new StarWarsApiService();
            var allStarships = await grabStarships.GetAllStarships();

            //Sort alphabetically
            var sortedAllStarships = allStarships.OrderBy(a => a.Name).ToList();

            if (sortedAllStarships != null)
            {
                StarshipOc = new ObservableCollection<Starship>();
                foreach(var starship in sortedAllStarships)
                {
                    StarshipOc.Add(starship);
                }
            }
            else
            {
                // Unable to get starship data from Starwars API
                var alertNoData = new AlertConfig()
                {
                    Title = "Error",
                    Message = "Unable to load data. Please check connectivity and try again"
                };
                UserDialogs.Instance.Alert(alertNoData);
                Console.WriteLine("Could not grab Starship data in MainPageVM");
            }
        }

        private async void HandleSelectedItem()
        {
            await _navigation.PushAsync(new StarshipView(ListViewItemSelectedBinding));
        }
    }
}
