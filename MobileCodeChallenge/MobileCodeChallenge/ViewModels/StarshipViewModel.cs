﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using MobileCodeChallenge.Models;
using MobileCodeChallenge.Services;

namespace MobileCodeChallenge.ViewModels
{
    public class StarshipViewModel : ViewModelBase
    {
        //Properties
        private Starship _starship;
        private bool activityIndicatorRun;
        public bool ActivityIndicatorRun
        {
            get
            {
                return activityIndicatorRun;
            }
            set
            {
                if (activityIndicatorRun != value)
                {
                    activityIndicatorRun = value;
                    OnPropertyChanged("ActivityIndicatorRun");
                }
            }
        }
        private Uri starshipImage;
        public Uri StarshipImage
        {
            get
            {
                return starshipImage;
            }
            set
            {
                if (starshipImage != value)
                {
                    starshipImage = value;
                    OnPropertyChanged("StarshipImage");
                }
            }
        }
        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (name != value)
                {
                    name = value;
                    OnPropertyChanged("Name");
                }
            }
        }
        private string model;
        public string Model
        {
            get
            {
                return model;
            }
            set
            {
                if (model != value)
                {
                    model = value;
                    OnPropertyChanged("Model");
                }
            }
        }
        private string starshipClass;
        public string StarshipClass
        {
            get
            {
                return starshipClass;
            }
            set
            {
                if (starshipClass != value)
                {
                    starshipClass = value;
                    OnPropertyChanged("StarshipClass");
                }
            }
        }
        private string manufacturer;
        public string Manufacturer
        {
            get
            {
                return manufacturer;
            }
            set
            {
                if (manufacturer != value)
                {
                    manufacturer = value;
                    OnPropertyChanged("Manufacturer");
                }
            }
        }
        private string costInCredits;
        public string CostInCredits
        {
            get
            {
                return costInCredits;
            }
            set
            {
                if (costInCredits != value)
                {
                    costInCredits = value;
                    OnPropertyChanged("CostInCredits");
                }
            }
        }
        private string length;
        public string Length
        {
            get
            {
                return length;
            }
            set
            {
                if (length != value)
                {
                    length = value;
                    OnPropertyChanged("Length");
                }
            }
        }
        private string crew;
        public string Crew
        {
            get
            {
                return crew;
            }
            set
            {
                if (crew != value)
                {
                    crew = value;
                    OnPropertyChanged("Crew");
                }
            }
        }
        private string passengers;
        public string Passengers
        {
            get
            {
                return passengers;
            }
            set
            {
                if (passengers != value)
                {
                    passengers = value;
                    OnPropertyChanged("Passengers");
                }
            }
        }
        private string maxAtmospheringSpeed;
        public string MaxAtmospheringSpeed
        {
            get
            {
                return maxAtmospheringSpeed;
            }
            set
            {
                if (maxAtmospheringSpeed != value)
                {
                    maxAtmospheringSpeed = value;
                    OnPropertyChanged("MaxAtmospheringSpeed");
                }
            }
        }
        private string hyperDriveRating;
        public string HyperDriveRating
        {
            get
            {
                return hyperDriveRating;
            }
            set
            {
                if (hyperDriveRating != value)
                {
                    hyperDriveRating = value;
                    OnPropertyChanged("HyperDriveRating");
                }
            }
        }
        private string mglt;
        public string Mglt
        {
            get
            {
                return mglt;
            }
            set
            {
                if (mglt != value)
                {
                    mglt = value;
                    OnPropertyChanged("Mglt");
                }
            }
        }
        private string cargo;
        public string Cargo
        {
            get
            {
                return cargo;
            }
            set
            {
                if (cargo != value)
                {
                    cargo = value;
                    OnPropertyChanged("Cargo");
                }
            }
        }
        private string consumables;
        public string Consumables
        {
            get
            {
                return consumables;
            }
            set
            {
                if (consumables != value)
                {
                    consumables = value;
                    OnPropertyChanged("Consumables");
                }
            }
        }

        public StarshipViewModel(Starship selectedStarship)
        {
            _starship = selectedStarship;
            Name = _starship.Name;
            Model = _starship.Model;
            StarshipClass = _starship.StarshipClass;
            Manufacturer = _starship.Manufacturer;
            CostInCredits = _starship.CostInCredits;
            Length = _starship.Length;
            Crew = _starship.Crew;
            Passengers = _starship.Passengers;
            MaxAtmospheringSpeed = _starship.MaxAtmospheringSpeed;
            HyperDriveRating = _starship.HyperdriveRating;
            Mglt = _starship.MGLT;
            Cargo = _starship.CargoCapacity;
            Consumables = _starship.Consumables;

            ActivityIndicatorRun = true;
        }
        public async Task LoadStarshipImage()
        {
            var grabStarShipImage = new StarshipImageService();
            try
            {
                var starshipImage = await grabStarShipImage.GetImageUrl(_starship.Name);
                if(string.IsNullOrEmpty(starshipImage))
                {
                    // Unable to get starship image
                    var alertNoData = new AlertConfig()
                    {
                        Title = "Error",
                        Message = "Unable to load data. Please check connectivity and try again"
                    };
                    UserDialogs.Instance.Alert(alertNoData);
                }
                var starShipImageUri = new Uri(starshipImage);

                StarshipImage = starShipImageUri;
            }
            catch
            {
                // Unable to get starship image
                var alertNoData = new AlertConfig()
                {
                    Title = "Error",
                    Message = "Unable to load data. Please check connectivity and try again"
                };
                UserDialogs.Instance.Alert(alertNoData);
                Console.WriteLine("Could not grab Starship image in StarshipVM");
            }
        }
    }
}
